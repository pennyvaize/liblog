from django.shortcuts import render, redirect
from .models import Book, Review
from .forms import BookForm, ReviewForm


# Create your views here.


def index(request):
    """Головна сторінка Журналу бібліотеки"""
    return render(request, 'lib_logs/index.html')


def books(request):
    """Показує всі книги"""
    books = Book.objects.order_by('data_added')
    context = {'books': books}
    return render(request, 'lib_logs/books.html', context)


def book(request, book_id):
    """Показує одну книгу та усі відгуки до неї"""
    book = Book.objects.get(id=book_id)
    reviews = book.review_set.order_by('-date_added')
    context = {'book': book, 'reviews': reviews}
    return render(request, 'lib_logs/book.html', context)

def new_book(request):
    '''Додаємо нову книгу'''
    if request.method != 'POST':
        # Жодних даних невідправлено - створити порожню форму.
        form = BookForm()
    else:
        # відправлений POST; обробити дані
        form = BookForm(data=request.POST)
        if form.is_valid():
            form.save()
            return redirect('lib_logs:books')

    # Показати порожню або недійсну форму.
    context = {'form': form}
    return render(request, 'lib_logs/new_book.html', context)

def new_review(request, book_id):
    '''Додаємо новий відгук до існуючої книги'''
    book=Book.objects.get(id=book_id)
    if request.method != 'POST':
        # Жодних даних невідправлено - створити порожню форму.
        form = ReviewForm()
    else:
        # відправлений POST; обробити дані
        form = ReviewForm(data=request.POST)
        if form.is_valid():
            new_review = form.save(commit=False)
            new_review.book = book
            new_review.save()
            return redirect('lib_logs:books', book_id=book_id)

    # Показати порожню або недійсну форму.
    context = {'book':book, 'form':form}
    return render(request, 'lib_logs/new_book.html', context)

def edit_review(request, review_id):
    review = Review.objects.get(id=review_id)
    book = review.book

    if request.method != 'POST':
        form = ReviewForm(instance=review)
    else:
        form = ReviewForm(instance=review, data=request.POST)
        if form.is_valid():
            form.save()
            return redirect('lib_logs:book', book_id=book.id)
    context = {'review': review, 'book': book, 'form': form}
    return render(request, 'lib_logs/edit_review.html', context)
